use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, BTreeMap}; // Import BTreeMap for lexicographical sorting

#[derive(Deserialize, Serialize)]
struct Input {
    data: String,
}

#[derive(Deserialize, Serialize)]
struct Output {
    result: BTreeMap<char, usize>, // Use BTreeMap for lexicographical sorting
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(process_data);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn process_data(event: Input, _ctx: Context) -> Result<Output, Error> {
    let mut result = HashMap::new();

    for c in event.data.chars() {
        // Ignore non-alphabetic characters
        if c.is_alphabetic() {
            let counter = result.entry(c.to_ascii_lowercase()).or_insert(0);
            *counter += 1;
        }
    }

    // Sort the result map lexicographically
    let sorted_result: BTreeMap<_, _> = result.into_iter().collect();

    Ok(Output {
        result: sorted_result,
    })
}
