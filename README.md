# IDS721 Mini-Project 2

## Author: Ziyu Shi, NetID: zs148

## Functionality
In this project, I have completed an AWS lambda function that calculates the frequency of every character in the input json file or sentence and returns the result in lexicographical order.

## Instruction guidance
To use this function, please follow the steps:
### Step 1: Install Cargo Lambda
```
cargo install cargo-lambda
```

### Step 2: Git Clone from GitLab Repo
```
git clone git@gitlab.com:JackShi123/ids721-mini2.git
```
### Step3: Run the Function in Terminal
1. Go into lamdda_funciton directory 
```
cd lambda_funciton
````
2. Invoke server by
```
cargo lambda watch
```
### Step3: Test in Terminal
1. Test with the json file by
```
cargo lambda invoke --data-file input.json
``` 
2. Test with the command by
```
cargo lambda invoke --data-ascii "{ \"data\": \"What a nice day\"}"
```


### Step4: Deploy
1. Build the function
```
cargo lambda build --release
```
2. Create an AWS account and generate access key, use `aws configure` to set key and region
3. Create a new IAM role
4. Deploy this function to your AWS by
```
cargo lambda deploy --region <YOUR_REGION> --iam-role arn:aws:iam::<YOUR_ACCOUNT_ID>:role/<YOUR_ROLE_NAME>
```

### Step5: Test the API Gateway by
```
curl -X POST https://qx4bf0p7ki.execute-api.us-east-2.amazonaws.com/lambda_function_stage \
  -H 'content-type: application/json' \
  -d '{ "data": "What a nice day!"}'
```

## Diagrams
### Test in Terminal
1. Test with json file 

The test file "input.json" is a comment about the "Gone With the Wind". The lambda function will calculate the frequency of every character in this comment and display the result in lexicographical order in the terminal.

![sample image](/lambda_function/photos/sample.png)

2. Test with command line

![sample image](/lambda_function/photos/sample1.png)

### Test in AWS

![sample image](/lambda_function/photos/sample2.png)

### Test the API Gateway
![sample image](/lambda_function/photos/sample3.png)

### My API Gateway Integration
![sample image](/lambda_function/photos/sample4.png)




